let express = require ('express');
const bodyParser = require('body-parser');
const PORT = 4000;
let app = express();
app.use(bodyParser.json());


const data = {
	users: []
}

app.get("/home",(req, res)=> res.send(`Welcome to the home page!`))
app.post("/signup",(req, res)=> {
	if (req.body.username!= "" && req.body.password != "") {
		const {username, password} = req.body;
		data.users.push({
			username: username,
			password: password,
			joined: new Date()
		})
		res.json(`User ${req.body.username} successfully registered!`)
		console.log(data)
	} else {
		res.status(400).json(`Pls input BOTH username and password.`)
	}
});
app.get("/users",(req, res) => res.send (data.users));

app.listen(PORT,()=>{
	console.log(`Server is running at ${PORT}`)
});